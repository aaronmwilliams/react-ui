import React, {Component} from 'react';

class UpdateStateMessage extends Component {
    render() {
    return (
        <span data-qa="state-button-update">
            Type in the field below to change the state of App component...
        </span>
    );
    }
}

export default UpdateStateMessage;
