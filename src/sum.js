import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Sum extends Component {
    render() {
        return (
            <div>
                <p>
                    These props types are validated as a number type.
                    Check JS console for type warning
                    as we have passed in a string
                    which is why we get 1 + 2 = 12!
                </p>
                <h3>
                    {this.props.a}
                        + {this.props.b} = {this.props.a + this.props.b}
                </h3>
            </div>
        );
    }
}
Sum.propTypes = {
    a: PropTypes.number.isRequired,
    b: PropTypes.number.isRequired,
};

export default Sum;
