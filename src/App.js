import React, {Component} from 'react';
import ReactGIF from './react-gif';
import UpdateStateMessage from './update-state-message';
import Sum from './sum';
import './bootstrap.min.css';
import {Link} from 'react-router-dom';

class App extends Component {
  constructor() {
    super();
    {
      this.state = {
        defaultState: 'This is the state of the component and can be changed',
      };
    }
    this.updateState = this.updateState.bind(this);
  }

  updateState(userInput) {
    this.setState({
      defaultState: 'Default state has changed to: ' + userInput,
    });
  }

  render() {
    return (
      <div className="jumbotron col-10 offset-1">
        <ReactGIF />
      <h3>Hello, {this.props.props}, and this cannot be changed!</h3>
      <br />
      <h3>{this.state.defaultState}</h3>
      <UpdateStateMessage />
      <br />
      <br />
      <input style={{height: '100px',
                                width: '100%',
                                font: '5.2em "Fira Sans", sans-serif'}}
                                onChange={(evt) => {
                                  this.updateState(evt.target.value);
                                }} data-qa="input" />
      <br />
      <br />
      <Sum a={'1'} b={2} />
      <br />
      <br />
      <p><Link to="/anotherpage">Click here to go to another page! :D</Link></p>
      </div>
    );
  }
}

export default App;
