import React from 'react';
import {shallow} from 'enzyme';

import App from './App';

describe('when updateState is called', () => {
    const wrapper = shallow(<App />);

    it('should update state with userinput', () => {
        const mockedUserInput = 'mocked user input';

        wrapper.find('[data-qa="input"]')
                .prop('onChange')({target: {value: mockedUserInput}});

        expect(wrapper.state().defaultState)
            .toEqual('Default state has changed to: '
                + mockedUserInput);
    });
});
