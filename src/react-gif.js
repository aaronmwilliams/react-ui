import React, {Component} from 'react';

class ReactGIF extends Component {
    render() {
    return (
        <div>
            <iframe src="https://giphy.com/embed/qm2CRlXb0gEx2" width="100%" height="218" frameBorder="0" className="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/original-content-qm2CRlXb0gEx2"></a></p>
        </div>
    );
    }
}

export default ReactGIF;
