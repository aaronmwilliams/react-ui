import React from 'react';
import {shallow} from 'enzyme';

import UpdateStateMessage from './update-state-message';

describe('<UpdateStateMessage />', () => {
    const componentElement = '[data-qa="state-button-update"]';
    const wrapper = shallow(<UpdateStateMessage />);

  it('renders with expected element', () => {
      expect(wrapper.find('[data-qa="state-button-update"]').exists())
      .toBe(true);
    });

    it('renders with expected text', () => {
        const expectedContent = 'Click the button below to change the state.';
        expect(wrapper.find(componentElement).contains(expectedContent))
            .toBe(true);
    });
});
