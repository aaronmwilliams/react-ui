import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter, Route, Link} from 'react-router-dom';

function MainPage() {
    return <App props="this is props inputted into the component" />;
}

function AnotherPage() {
    return <div>
                <p>YOU MADE IT HERE!</p>
                <Link to="/">Click here to go back!</Link>
            </div>;
}

ReactDOM.render(
    <BrowserRouter>
        <React.Fragment>
            <Route exact path="/" component={MainPage} />
            <Route path="/anotherpage" component={AnotherPage} />
         </React.Fragment>
    </BrowserRouter>,
    document.getElementById('root'));
