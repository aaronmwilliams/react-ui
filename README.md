# React UI

## Installing and Running
`npm install` then `npm start` to start the dev server.

The app will run on http://localhost:3000/

## Unit tests
`npm test`

The unit tests run using Jest and Enzyme for a virtual DOM.

## ESLint
Please make sure you have ESLint install globally and works with your IDE. Please do not push if there are errors or warning.